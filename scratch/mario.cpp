// mario.cpp
// driver program for the game

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include "screen.h"

using namespace std;

screen background;
SDL_Window* gWindow = NULL; // the window we will be rendering too
SDL_Renderer* gRenderer = NULL; // the window renderer

bool loadMedia(){
	bool success = true;
	if(!background.loadFromFile("blocks2.png")){
		cout << "Failed to load the background" << endl;
		success = false;
	}
	return success;
}

void close(){
	background.free();

	// Destroy the Window
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char* args[] ){
	screen background;
	
	if(!background.init()){
		cout << "Failed to initialize" << endl;
	}
	else{
		if(!loadMedia()){
			cout << "Failed to load media" << endl;
		}
		else{
			bool quit = false;
			int scrollingOffset = 0;

			while(!quit){
				--scrollingOffset;
				if( scrollingOffset < -background.getWidth()){
					scrollingOffset = 0;
				}
				// Clear Screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );
				// Render background
				background.render( scrollingOffset, 0);
				background.render( scrollingOffset + background.getWidth(), 0);
				// Update Screen
				SDL_RenderPresent( gRenderer );
				
			}
		}
	}
	// Free Resources and Close SDL
	close();
return 0;
}
