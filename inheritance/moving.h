#ifndef MOVING_H
#define MOVING_H
#include "screen.h"   // include header of base class

using namespace std;

class Moving : public Screen {	// moving class inherits parent function

	public:

		Moving();	//constructor

	private:
		
		int xVel;
		int yVel;
};

#endif
