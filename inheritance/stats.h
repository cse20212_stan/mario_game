#ifndef STATS_H
#define STATS_H
#include "nonmoving.h"   // include header of base class

using namespace std;

class Stats : public NonMoving {	// stats class inherits parent function

	public:

		Stats();	//constructor
		void setTime();
		int getTime();
		void setLives();
		int getLives();
		void setCoins();
		int getCoins();

	private:

		int coins;
		int lives;
		int time;
};

#endif
