#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H
#include "nonmoving.h"   // include header of base class

using namespace std;

class Environment : public NonMoving {	// environment class inherits parent function

	public:

		Environment();	//constructor

	private:

};

#endif
