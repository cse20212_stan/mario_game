#ifndef ENEMY_H
#define ENEMY_H
#include "character.h"   // include header of base class

using namespace std;

class Enemy : public Character {	// enemy class inherits parent function

	public:

		Enemy();	//constructor

	private:
	
		int leftBound;
		int rightBound;
		int upperBound;
		int lowerBound;

};

#endif
