#ifndef NONMOVING_H
#define NONMOVING_H
#include "screen.h"   // include header of base class

using namespace std;

class NonMoving : public Screen {	// non-moving class inherits parent function

	public:

		NonMoving();	//constructor

	private:

};

#endif
