// Mario.h
// This is the class for mario, should hold his position and load the sprite for mario
//
#ifndef MARIO_H
#define MARIO_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <iostream>
#include "Constants.h"
#include "LTexture.h"
#include "Wall.h"
#include "Goomba.h"

using namespace std;
class Mario : public Constants {
	public:
		Mario(int, int, int);
		void handleEvent( SDL_Event& e); // Takes Key Presses and Adjust Mario's Velocity
		int move(vector<Wall> & walls, int, int); // Moves Mario without the collision detection	
		void Mrender( SDL_Rect* clip = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE); // shows mario on the screen
		void renderCurrent(int);
		
		// gets the position
		int getX();
		int getY();
		
		// Change the animation value
		int getAnimation();
		void setAnimation(int);
		
		// Change flip value
		bool getFlip();
		void setFlip(bool);
		
		// gets the velocity of Mario
		int getVelX();
		int getVelY();	
		int getdx();	
		
		// SDL
		bool loadMario();
		void close();
 
		//Box Collision Detection
		bool checkAnyCollision(SDL_Rect , SDL_Rect );
		bool checkCeilingCollision( SDL_Rect, SDL_Rect );
		bool checkGroundCollision( SDL_Rect, SDL_Rect );
		bool checkWallCollisions(SDL_Rect, vector<Wall> &, int);

		// animation
		SDL_Rect gSpriteClips[ MARIO_ANIMATION_FRAMES ];
		LTexture gSpriteSheetTexture;
		LTexture gMarioTexture;

		SDL_Rect getCollider();
		
		bool getBig();
		void setBig(bool);
		int getDead();
		void setDead(int);
		void setMaxJump(int);
		bool getVictory();
		void setVictory(bool);
		bool getWin();
		void setWin(bool);

	private:
		int SCREEN_WIDTH;
		int SCREEN_HEIGHT;
		int mPosX, mPosY; // x and y offsets of mario
		double mVelX, mVelY;
		int dx;
		int level;
		
		SDL_Rect mCollider; // Mario's Collision Box;

		// Animation
		int animation;
		bool right, left, up, down;
		SDL_RendererFlip flipType = SDL_FLIP_NONE;
		bool flip;
		// Jumping
		int maxJump;
		int jumpsLeft;

		bool big;
		int dead;
		bool victory;	// for sliding down pole animations
		bool win;		// for ending level with win
};

Mario::Mario(int width, int height, int lev){
	SCREEN_WIDTH = width;
	SCREEN_HEIGHT = height;
	level = lev;
	
	// Initialize the offsets
	mPosX = SCREEN_WIDTH/2;
	mPosY = 0;
	
	// Set Collision Box Dimensions
	mCollider.w = Mario_WIDTH;
	mCollider.h = Mario_HEIGHT;

	// initialize the velocity
	mVelX = SCROLL;
	mVelY = 0;
	
	// Image Variables
	animation = 0;
	right = 0;
	left = 0;
	up = 0;
	down = 0;
	flipType = SDL_FLIP_NONE;
	maxJump = 0;
	jumpsLeft = 2;
	
	victory = false;
	win = false;
	setBig(false);
	dead = 0;
}

bool Mario::checkAnyCollision( SDL_Rect a, SDL_Rect b )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    //Calculate the sides of rect B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;

    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

bool Mario::checkCeilingCollision( SDL_Rect a, SDL_Rect b )
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	int overlap1, overlap2;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	// If there is no collision
	if( bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB )
		return false;

	// If Mario collides with ceiling
	if( topA <= bottomB && bottomA >= topB )
		overlap1 = max(0, min(rightA, rightB) - max(leftA, leftB));
	else	// Mario collided with side
		overlap1 = 0;
	overlap2 = max(0, min(bottomA, bottomB) - max(topA, topB));
	
	if( overlap1 >= overlap2 )
		return true;
	else
		return false;
}

bool Mario::checkGroundCollision( SDL_Rect a, SDL_Rect b )
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	int overlap1, overlap2;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	// If there is no collision
	if( bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB )
		return false;

	// If Mario collides with ground
	if( bottomA >= topB && topA <= topB )
		overlap1 = max(0, min(rightA, rightB) - max(leftA, leftB));
	else	// Mario collided with side
		overlap1 = 0;
	overlap2 = max(0, min(bottomA, bottomB) - max(topA, topB));
	
	if( overlap1 >= overlap2 )
		return true;
	else
		return false;
}

bool Mario::checkWallCollisions(SDL_Rect character, vector<Wall> & walls, int side) {
	switch(side){
		case 1:		// Any
			for(int i = 0; i < walls.size(); i++) {
				SDL_Rect obstacle = walls[i].getCollider();
				if(checkAnyCollision(character, obstacle))
					return true;
			}
			return false;
		case 2:		// Ceiling
			for(int i = 0; i < walls.size(); i++) {
				SDL_Rect obstacle = walls[i].getCollider();
				if(checkCeilingCollision(character, obstacle))
					return true;
			}
			return false;
		case 3:		// Ground
			for(int i = 0; i < walls.size(); i++) {
				SDL_Rect obstacle = walls[i].getCollider();
				if(checkGroundCollision(character, obstacle))
					return true;
			}
			return false;
	}
}

void Mario::handleEvent( SDL_Event& e )
{
   //If a key was pressed
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 )
	{
		//Adjust the velocity
		switch( e.key.keysym.sym )
		{
			case SDLK_UP:
				if(jumpsLeft){
					up = 1;
					down = 0;
					maxJump = mPosY - JUMP;
					jumpsLeft--;
				}
				break;
			case SDLK_DOWN:
				down = 1;
				up = 0;
				if(big){
					mPosY += (BigMario_HEIGHT - BigMario_DuckHeight);
					mCollider.h = BigMario_DuckHeight;
				}
				else {
					mPosY += (Mario_HEIGHT - Mario_DuckHeight);
					mCollider.h = Mario_DuckHeight;
				}
				break;
			case SDLK_LEFT:
				left = 1;
				right = 0;
				break;
			case SDLK_RIGHT:
				right = 1;
				left = 0;
				break;
		}
	}
	//If a key was released
	else if( e.type == SDL_KEYUP && e.key.repeat == 0 )
	{
		//Adjust the velocity
		switch( e.key.keysym.sym )
		{
			case SDLK_UP:
				up = 0;
				break;
			case SDLK_DOWN: 
				down = 0;
				if(big){
					mPosY -= (BigMario_HEIGHT - BigMario_DuckHeight);
					mCollider.h = BigMario_HEIGHT;
				}
				else {
					mPosY -= (Mario_HEIGHT - Mario_DuckHeight);
					mCollider.h = Mario_HEIGHT;
				}
				break;
			case SDLK_LEFT:
				left = 0;
				break;
			case SDLK_RIGHT:
				right = 0;
				break;
		}
	}
	if(left){
		flip = 1;
		mVelX = -Mario_VelX; // + SCROLL; 
		animation = 2;
	}
	else if(right){
		flip = 0;
		mVelX = Mario_VelX;
		animation = 2;
	}
	else {
		mVelX = 0; // SCROLL;
		animation = 1;
	}
	
	if(up){
		mVelY = -2 * Mario_VelY; 
		animation = 3;
	}
	else if(down){
		mVelY = 2 * Mario_VelY;
		animation = 4;
	}
	else {
		mVelY = 2 * Mario_VelY;
	}
	
	if(mPosY <= maxJump){
		up = 0;
		maxJump = 0;
	}
}

int Mario::move(vector<Wall> & walls, int beginningOffset, int endOffset)
{
	if( dead == 1 )
	{
		if( mPosY <= maxJump ) {
			maxJump = 0;
			mPosY += 2 * Mario_VelY;
		}
		else if ( maxJump != 0 )
			mPosY -= 2 * Mario_VelY;
		else
			mPosY += 2 * Mario_VelY;
	}
	else if(victory){
		switch(level){
			case 1:
				if(mPosY < 134){
					mPosY += 1;
					animation = 5;
				}
				else{
					animation = 2;
					mPosX += Mario_VelX;
					mPosY += 1;
					mCollider.y = mPosY;
					if( checkWallCollisions(mCollider,walls, 1) )
					{
						mPosY -= 1;
						mCollider.y = mPosY;
					}
				}
				break;
			case 2:
				if(mPosY < 402){
					mPosY += 1;
					animation = 5;
				}
				else{
					animation = 2;
					mPosX += Mario_VelX;
					mPosY += 2;
					mCollider.y = mPosY;
					if( checkWallCollisions(mCollider,walls, 1) )
					{
						mPosY -= 1;
						mCollider.y = mPosY;
					}
				}
				break;
		}
	}
	else{
		//Move the dot left or right		
		if( ( beginningOffset - mVelX >= 0 || mPosX < SCREEN_WIDTH / 2 ) ){
			mPosX += mVelX;
			mCollider.x = mPosX;
			if( !checkWallCollisions(mCollider,walls, 1) ){
				dx = 0;
			}
			else{
				dx = 0;
				mPosX -= mVelX;
				mCollider.x = mPosX;
			}
		}
		else if( ( endOffset - mVelX < SCREEN_WIDTH - 2048 || mPosX > SCREEN_WIDTH / 2 ) ){
			mPosX += mVelX;
			mCollider.x = mPosX;
			if ( !checkWallCollisions(mCollider,walls, 1) ){
				dx = 0;
			}
			else{
				dx = 0;
				mPosX -= mVelX;
				mCollider.x = mPosX;
			}
		}
		else{
			mPosX += mVelX;
			mCollider.x = mPosX;
			if ( !checkWallCollisions(mCollider,walls, 1) )
				dx = mVelX;
			else
				dx = 0;
			mPosX -= mVelX;
			mCollider.x = mPosX;
		}

		//If the dot went too far to the left or right
		if( ( mPosX + Mario_WIDTH > SCREEN_WIDTH ) || checkWallCollisions(mCollider,walls, 1) )
		{
			dx = 0;
		}

		//Move the dot up or down
		mPosY += mVelY;
		mCollider.y = mPosY;
		
		if( checkWallCollisions(mCollider,walls, 2) )
			maxJump = SCREEN_HEIGHT;
		
		if( checkWallCollisions(mCollider,walls, 3) ){
			jumpsLeft = 2;
			
		}

		// makes Mario move on the ground
		if( checkWallCollisions(mCollider,walls, 1) )
		{
			//Move back
			mPosY -= mVelY;
			mCollider.y = mPosY;
		}
	}

	// Mario dies if he goes off the screen
	if( mPosY - 10 > SCREEN_HEIGHT )
		dead = 2;
	else if( mPosX + 10 > SCREEN_WIDTH )
		win = true;
}

void Mario::Mrender(SDL_Rect* clip, SDL_RendererFlip flip)
{
	gMarioTexture.render( getX(), getY(), clip, NULL, NULL, flip );
}

int Mario::getX(){
	return mPosX;
}

int Mario::getY(){
	return mPosY;
}

int Mario::getAnimation(){
	return animation;
}

void Mario::setAnimation(int val){
	animation = val;
}

bool Mario::getFlip(){
	return flip;
}

void Mario::setFlip(bool val){
	flip = val;
}

bool Mario::loadMario(){
	//Loading success flag
	bool success = true;
	
	//Load press texture
	if( !gMarioTexture.loadFromFile( "../spritesheet.bmp","white" ) )
	{
		printf( "Failed to load Mario texture!\n" );
		success = false;
	}
	else
	{
		// If not big
			// Standing - Running 0-3
			gSpriteClips[ 0 ].x = 13;
			gSpriteClips[ 0 ].y = 9;
			gSpriteClips[ 0 ].w = 15;
			gSpriteClips[ 0 ].h = 21;
		
			gSpriteClips[ 1 ].x = 30;
			gSpriteClips[ 1 ].y = 9;
			gSpriteClips[ 1 ].w = 15;
			gSpriteClips[ 1 ].h = 21;
		
			gSpriteClips[ 2 ].x = 47;
			gSpriteClips[ 2 ].y = 9;
			gSpriteClips[ 2 ].w = 15;
			gSpriteClips[ 2 ].h = 21;

			gSpriteClips[ 3 ].x = 64;
			gSpriteClips[ 3 ].y = 9;
			gSpriteClips[ 3 ].w = 15;
			gSpriteClips[ 3 ].h = 21;
			// Jumping
			gSpriteClips[ 4 ].x = 115;
			gSpriteClips[ 4 ].y = 7;
			gSpriteClips[ 4 ].w = 15;
			gSpriteClips[ 4 ].h = 21;
			// Ducking
			gSpriteClips[ 5 ].x = 167;
			gSpriteClips[ 5 ].y = 16;
			gSpriteClips[ 5 ].w = 15;
			gSpriteClips[ 5 ].h = 15;
			// Dying
			gSpriteClips[ 6 ].x = 200;
			gSpriteClips[ 6 ].y = 9;
			gSpriteClips[ 6 ].w = 15;
			gSpriteClips[ 6 ].h = 20;
		
		// If big
			// Standing - Running 7-10
			gSpriteClips[ 7 ].x = 13;
			gSpriteClips[ 7 ].y = 60;
			gSpriteClips[ 7 ].w = 17;
			gSpriteClips[ 7 ].h = 29;
		
			gSpriteClips[ 8 ].x = 30;
			gSpriteClips[ 8 ].y = 60;
			gSpriteClips[ 8 ].w = 17;
			gSpriteClips[ 8 ].h = 29;
		
			gSpriteClips[ 9 ].x = 47;
			gSpriteClips[ 9 ].y = 60;
			gSpriteClips[ 9 ].w = 17;
			gSpriteClips[ 9 ].h = 29;

			gSpriteClips[ 10 ].x = 30;
			gSpriteClips[ 10 ].y = 60;
			gSpriteClips[ 10 ].w = 17;
			gSpriteClips[ 10 ].h = 29;
			// Jumping
			gSpriteClips[ 11 ].x = 155;
			gSpriteClips[ 11 ].y = 60;
			gSpriteClips[ 11 ].w = 17;
			gSpriteClips[ 11 ].h = 29;
			// Ducking
			gSpriteClips[ 12 ].x = 215;
			gSpriteClips[ 12 ].y = 77;
			gSpriteClips[ 12 ].w = 15;
			gSpriteClips[ 12 ].h = 14;
			// Dying
			gSpriteClips[ 13 ].x = 251;
			gSpriteClips[ 13 ].y = 63;
			gSpriteClips[ 13 ].w = 27;
			gSpriteClips[ 13 ].h = 30;
			// normal victory
			gSpriteClips[ 14 ].x = 283;
			gSpriteClips[ 14 ].y = 33;
			gSpriteClips[ 14 ].w = 17;
			gSpriteClips[ 14 ].h = 21;
			// big victory
			gSpriteClips[ 15 ].x = 463;
			gSpriteClips[ 15 ].y = 95;
			gSpriteClips[ 15 ].w = 19;
			gSpriteClips[ 15 ].h = 29;
	}
}

void Mario::renderCurrent(int frame){
	SDL_RendererFlip flipType = SDL_FLIP_NONE; 

	if(getFlip())
		flipType = SDL_FLIP_HORIZONTAL;
	else
		flipType = SDL_FLIP_NONE;

	// Render current frame
	SDL_Rect* currentClip = &gSpriteClips[ 0 ];
	if(dead == 1 && !big)
		currentClip = &gSpriteClips[ 6 ];
	else if(dead == 1 && big)
		currentClip = &gSpriteClips[ 13 ];
	else if(!big){
		switch(getAnimation()){
			case 1:	// standing
				currentClip = &gSpriteClips[ 0 ];
				break;
			case 2:	// running
				currentClip = &gSpriteClips[ frame % 4 ];
				break;
			case 3:	// jumping
				currentClip = &gSpriteClips[ 4 ];
				break;
			case 4:	// ducking
				currentClip = &gSpriteClips[ 5 ];
				break;
			case 5:	// victory
				currentClip = &gSpriteClips[ 14 ];
				break;
		}
	}
	else{
		switch(getAnimation()){
			case 1:	// standing
				currentClip = &gSpriteClips[ 7 ];
				break;
			case 2:	// running
				currentClip = &gSpriteClips[ (frame % 4) + 7 ];
				break;
			case 3:	// jumping
				currentClip = &gSpriteClips[ 11 ];
				break;
			case 4:	// ducking
				currentClip = &gSpriteClips[ 12 ];
				break;
			case 5:	// victory
				currentClip = &gSpriteClips[ 15 ];
				break;
		}
	}

	Mrender( currentClip, flipType); // render mario
}

void Mario::close(){
	gMarioTexture.free();
}

SDL_Rect Mario::getCollider()
{
	//Render wall
	return mCollider;
}

void Mario::setBig(bool val){
	big = val;
	if(val) {
		mPosY -= (BigMario_HEIGHT - Mario_HEIGHT);
		mCollider.h = BigMario_HEIGHT;
		mCollider.w = BigMario_WIDTH;
	}
	else {
		mPosY += (BigMario_HEIGHT - Mario_HEIGHT);
		mCollider.h = Mario_HEIGHT;
		mCollider.w = Mario_WIDTH;
	}
}

bool Mario::getBig(void){
	return big;
}

void Mario::setDead(int val){
	dead = val;
}

int Mario::getDead(void){
	return dead;
}

void Mario::setVictory(bool val){
	victory = val;
}

bool Mario::getVictory(void){
	return victory;
}

void Mario::setWin(bool val){
	win = val;
}

bool Mario::getWin(void){
	return win;
}

void Mario::setMaxJump(int val){
	maxJump = val;
}

int Mario::getdx(){
	return dx;
}

#endif
