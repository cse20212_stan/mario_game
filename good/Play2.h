// Play.h
// Program where the game is played
#ifndef PLAY_H
#define PLAY_H
//
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <iostream>
#include "LTexture.h"
#include "Constants.h"
#include "Mario.h"
#include "Wall.h"
#include "Goomba.h"

using namespace std;

class Play : public Constants {
	public:
		// constructor
		Play(int);

		// Deconstructor
		~Play();

		//Starts up SDL and creates window
		bool init();

		//Loads media
		bool loadMedia();
		bool loadMediaTwo(string, string, string);

		// Play main while loop
		void LevelOne();
		void LevelTwo();

		//Frees media and shuts down SDL
		void closeGame();

		//The window we'll be rendering to
		SDL_Window* gWindow;

		
		// Title Screen
		LTexture titleScreen;
		bool initTitle();
		bool loadTitle(); // load the picture
		void TitleScreen(); // driver function of the title screen

		// GameOver Screen
		LTexture gameoverScreen;
		bool initGameOver();
		bool loadGameOver(); // load the gameover png
		void GameOverScreen(); // driver function of game over
		
		// Background
		LTexture background;
		LTexture background1;
		LTexture background2;
		LTexture background3;
		
		int checkCollision(SDL_Rect, SDL_Rect);

		private:
			int defaultLives;
			int lives;
};

#endif



Play::Play(int val){
		//The window we'll be rendering to
		SDL_Window* gWindow = NULL;

		//Walking Animation?
		
		// Background
		LTexture background1;
		LTexture background2;
		LTexture background3;
		
		defaultLives = val;
		lives = val;
}

Play::~Play(){
}

// Title Screen
//TITLE SCREEN
bool Play::initTitle(){
    //Initialization flag
    bool success = true;
 
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else {
        //Set texture filtering to linear
        if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ) {
            printf( "Warning: Linear texture filtering not enabled!" );
        }
 
        //Create window
        gWindow = SDL_CreateWindow( "STAN's World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, TITLESCREEN_WIDTH, TITLESCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL ) {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else {
            //Create vsynced renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
            if( gRenderer == NULL ) {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else {
                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
 
                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }
            }
        }
    }
    return success;
}
 
 
bool Play::loadTitle(){
//Loading success flag
    bool success = true;
    //Load background texture
    if( !titleScreen.loadFromFile( "TitleScreen.png","white" ) ) {
        printf( "Failed to load background texture!\n" );
        success = false;
    }
    return success;
}
 
void Play::TitleScreen() {
    //Start up SDL and create window
    if( !initTitle() ) {
        printf( "Failed to initialize!\n" );
    }
    else{
        //Load Title Screen
        if( !loadTitle() ) {
            printf( "Failed to load title!\n" );
        }
        else{  
            //Main loop
            bool quit = false;
           
            //Event handler
            SDL_Event e;
           
            //While application is running
            while( !quit ) {
                //Handle events on queue
                while( SDL_PollEvent( &e ) != 0 ) {
                    if( e.type == SDL_KEYUP ) {//press UP ARROW to close TitleScreen and run LevelOne()
                        if(e.key.keysym.sym == SDLK_SPACE){
                            quit = true;
                            closeGame();
                        }
                    }
                    //User requests quit
                    if( e.type == SDL_QUIT ) {
                        quit = true;
                    }
                }
               
                //Clear screen
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
                SDL_RenderClear( gRenderer );
               
                //Render
                titleScreen.render( 0, 0 );
 
                //Update screen
                SDL_RenderPresent( gRenderer );
            }
        }
    }
    //Free resources and close SDL
    closeGame();
}


bool Play::init(){
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "STAN's World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool Play::loadMediaTwo(string FILE1, string FILE2, string FILE3)
{
   
    //Loading success flag
    bool success = true;
 
    // Load Sprite Clips Here?
    if( TTF_Init() == -1 ) { 
        printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() ); 
        success = false; 
    }
 
    //Open the font
    gFont = TTF_OpenFont( "./SuperMario256.ttf", 20 );
    if( gFont == NULL )
    {
        printf( "Failed to load Mario font! SDL_ttf Error: %s\n", TTF_GetError() );
        success = false;
    }
    else
    {
        //Render text
        SDL_Color textColor = { 0, 256, 0 };
        if( !gTextTexture.loadFromRenderedText( "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMARIO\t\t\t\t\t\t\t\t\t\t\tWORLD\t\t\t\t\t\t\t\t TIME", textColor ) )
        {
            printf( "Failed to render text texture!\n" );
            success = false;
        }
    }  
   
    //Load background texture
    if( !background1.loadFromFile( FILE1,"white" ) ){
        printf( "Failed to load background texture!\n" );
        success = false;
    }
    if( !background2.loadFromFile( FILE2,"white" ) ){
        printf( "Failed to load background texture!\n" );
        success = false;
    }
    if( !background3.loadFromFile( FILE3,"white" ) ){
        printf( "Failed to load background texture!\n" );
        success = false;
    }
   
    return success;
}

void Play::LevelTwo(){
	// initialize all of the variables for mario
	//Start up SDL and create window
	if( !init() ){
		printf( "Failed to initialize!\n" );
	}
	else{
		//Load media
		if( !loadMediaTwo("level1.bmp", "level2.bmp", "level3.bmp") ){
			printf( "Failed to load media!\n" );
		}
		else{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			// Instantiation of Different Objects goes after this
			Mario mario;	
			
			// Instantiation of goombas
			Goomba goomba(600,Goomba_GROUND, 524, 857);
			Goomba goomba2(800,Goomba_GROUND, 500, 1000);
			Goomba goomba3(1200,Goomba_GROUND, 1000, 1300);
			
			vector<Goomba> goombasVec {
				goomba, goomba2, goomba3
			};
			
			mario.loadMario();
			goomba.loadGoomba();

			//The background scrolling offset
			int scrollingOffset1 = 0;
			int scrollingOffset2 = 2048;
			int scrollingOffset3 = 4096;

			//Current animation frame
			int frame = 0;
			int gframe = 0;
			
			//Set the wall
			Wall wall1(0, 431, 1077, 74);
			Wall wall2(292, 399, 63, 32);
			Wall wall3(461, 399, 32, 32);
			Wall wall4(325,380,31,20);
			Wall wall5(494,367,31,64);
			Wall wall6(83,332,77,13);
			Wall wall7(617,308,85,13);
			Wall wall8(874,405,42,25);
			Wall wall9(1078,399,159,105);
			Wall wall10(1110,380,127,21);
			Wall wall11(1236,401,86,103);
			Wall wall12(1322,380,70,124);
			Wall wall13(1242,297,78,13);
			Wall wall14(1258,214,30,13);
			Wall wall15(1502,308,69,13);
			Wall wall16(1676,218,80,15);
			Wall wall17(762,121,10,10);
			Wall wall18(771,83,80,10);
			Wall wall19(878,83,80,10);
			Wall wall20(908,83,10,10);
			Wall wall21(908,121,10,10);
			Wall wall22(965,121,20,10);
			Wall wall23(1023,121,10,10);
			Wall wall24(1052,121,10,10);
			Wall wall25(1081,121,10,10);
			Wall wall26(1952,83,10,10);
			Wall wall27(1139,121,10,10);
			Wall wall28(1168,83,30,10);
			Wall wall29(1245,121,20,10);
			Wall wall30(1235,83,10,10);
			Wall wall31(1245,83,10,10);
			Wall wall32(1255,83,10,10);
			Wall wall33(1265, 83,10, 10);
			Wall wall34(1294,150,40,10);
			Wall wall35(1303,141,30,10);
			Wall wall36(1313,130,20,10);
			Wall wall37(1322,121,10,10);
			Wall wall38(1352,121,10,10);
			Wall wall39(1352,131,20,10);
			Wall wall40(1352,141,30,10);
			Wall wall41(1352,150,40,10);
			Wall wall42(1429,150,50,10);
			Wall wall43(1438,140,40,10);
			Wall wall44(1448,131,30,10);
			Wall wall45(1457,121,20,10);
			Wall wall46(1496,121,10,10);
			Wall wall47(1496,131,20,10);
			Wall wall48(1496,151,40,10);
			Wall wall49(1573,140,21,20);
			Wall wall50(1622,121,20,10);
			Wall wall51(1642,121,10,10);
			Wall wall52(1650,120,10,10);
			Wall wall53(1728,140,20,20);
			Wall wall54(1749,150,90,10);
			Wall wall55(1757,140,80,10);
			Wall wall56(1767,131,70,10);
			Wall wall57(1777,121,60,10);
			Wall wall58(1786,112,50,10);
			Wall wall59(1796,101,40,10);
			Wall wall60(1805,92,30,10);
			Wall wall70(1814,82,20,10);
			Wall wall71(1496,162,503,42);
			
			// Create the vector of boxes
			vector<Wall> wallsVec {
				wall1,
				wall2,
				wall3,
				wall4,
				wall5,
				wall6,
				wall7,
				wall8,
				wall9,
				wall10,
				wall11,
				wall12,
				wall13,
				wall14,
				wall15,
				wall16,
				/*wall17,
				wall18,
				wall19,
				wall20,
				wall21,
				wall22,
				wall23,
				wall24,
				wall25,
				wall26,
				wall27,
				wall28,
				wall29,
				wall30,
				wall31,
				wall32,
				wall33,
				wall34,
				wall35,
				wall36,
				wall37,
				wall38,
				wall39,
				wall40,
				wall41,
				wall42,
				wall43,
				wall44,
				wall45,
				wall46,
				wall47,
				wall48,
				wall49,
				wall50,
				wall51,
				wall52,
				wall53,
				wall54,
				wall55,
				wall56,
				wall57,
				wall58,
				wall59,
				wall60,
				wall70,
				wall71*/
			};

			//SDL_RendererFlip flipType = SDL_FLIP_NONE; 

			//While application is running
			while( !quit && mario.getDead() < 2 )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}

					//Handle input for the dot
					mario.handleEvent( e );
				}
				//for(int i = 0; i < goombasVec.size(); i++)
					//goombasVec[i].go(-mario.getdx());
				goomba.go(-mario.getdx());

				//Move the Objects
				//SDL_Rect obstacle = wall.getCollider();
				mario.move( wallsVec, scrollingOffset1, scrollingOffset3 );		// Pass in vector of objects to check collisions
				
				SDL_Rect mCollider = mario.getCollider();
				//for(int i = 0; i < goombasVec.size(); i++)
					//goombasVec[i].move( mCollider, -mario.getdx() );
				goomba.move(mCollider, -mario.getdx());
				for(int i = 0; i < wallsVec.size(); i++)
					wallsVec[i].move(-mario.getdx());

				//Scroll background
				scrollingOffset1 -= mario.getdx();
				scrollingOffset2 -= mario.getdx();
				scrollingOffset3 -= mario.getdx();

				//Clear screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );
				
				//Render background
				background1.render( scrollingOffset1, 0 );
				background2.render( scrollingOffset2, 0 );
				background3.render( scrollingOffset3, 0 );
				//background.render( scrollingOffset + background.getWidth(), 0 );
				
				//***************************************************************
				//*********************Draw Debugging Boxes**********************
				//***************************************************************
				for(int i = 0; i < wallsVec.size(); i++)
					wallsVec[i].render();
				SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );		
				SDL_RenderDrawRect( gRenderer, &mCollider );
				//***************************************************************
					
				if( !goomba.getDead() && checkCollision(mario.getCollider(), goomba.getCollider()) == 1)
					goomba.setDead(true);
				else if( !goomba.getDead() && checkCollision(mario.getCollider(), goomba.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}

					
				if( !goomba2.getDead() && checkCollision(mario.getCollider(), goomba2.getCollider()) == 1)
					goomba2.setDead(true);
				else if( !goomba2.getDead() && checkCollision(mario.getCollider(), goomba2.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}
					
				if( !goomba3.getDead() && checkCollision(mario.getCollider(), goomba3.getCollider()) == 1)
					goomba3.setDead(true);
				else if( !goomba3.getDead() && checkCollision(mario.getCollider(), goomba3.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}
				
				if(mario.getDead() != 2)
					mario.renderCurrent(frame);
				else { cout << "Lives: " << mario.getDead() << endl;
					lives--;
					quit = true;
				}

				if(!goomba.getDead())
					goomba.renderCurrent(gframe);
				if(!goomba2.getDead())
					goomba2.renderCurrent(gframe);
				if(!goomba3.getDead())
					goomba3.renderCurrent(gframe);


//Render current frame
				gTextTexture.render( ( 0 ) , ( 0 )  );

				//Update screen
				SDL_RenderPresent( gRenderer );

				// go to the next frame
				++frame;
				++gframe;
				
				// Cycle animation
				if( frame/4 >= 4){
					frame = 0;
				}
				if(gframe/2 >=GOOMBA_ANIMATION_FRAMES){
					gframe = 0;
				}
			}
cout << "ERROR CHECKING 1" << endl;
//mario.closeGame();
cout << "ERROR CHECKING 2" << endl;
//goomba.closeGame();
cout << "ERROR CHECKING 3" << endl;
//goomba2.closeGame();
cout << "ERROR CHECKING 4" << endl;
//goomba3.closeGame();
cout << "ERROR CHECKING 5" << endl;
		}
	}
	//Free resources and close SDL
closeGame();
cout << "ERROR CHECKING 6" << endl;		// Never gets to this check for some reason
}

int Play::checkCollision( SDL_Rect a, SDL_Rect b )
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	int overlap1, overlap2;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	// If there is no collision
	if( bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB )
		return 0;

	// If Mario kills goomba
	if( bottomA >= topB && topA <= topB )
		overlap1 = max(0, min(rightA, rightB) - max(leftA, leftB));
	// If goomba kills Mario
	else
		overlap1 = 0;
	overlap2 = max(0, min(bottomA, bottomB) - max(topA, topB));
	
	if( overlap1 >= overlap2 )
		return 1;
	else
		return 2;
}

// GAMEOVER SCREEN
bool Play::initGameOver(){
    //Initialization flag
    bool success = true;
 
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else {
        //Set texture filtering to linear
        if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ) {
            printf( "Warning: Linear texture filtering not enabled!" );
        }
 
        //Create window
        gWindow = SDL_CreateWindow( "STAN's World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, GAMEOVERSCREEN_WIDTH, GAMEOVERSCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL ) {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else {
            //Create vsynced renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
            if( gRenderer == NULL ) {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else {
                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
 
                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }
            }
        }
    }
    return success;
}
 
 
bool Play::loadGameOver(){
//Loading success flag
    bool success = true;
    //Load background texture
    if( !gameoverScreen.loadFromFile( "MarioGameOver.png","white" ) ) {
        printf( "Failed to load Game Over texture!\n" );
        success = false;
    }
    return success;
}
 
void Play::GameOverScreen() {
	//Start up SDL and create window
	if( !initGameOver() ) {
	    printf( "Failed to initialize!\n" );
	}
	else{
	    //Load Title Screen
	    if( !loadGameOver() ) {
		printf( "Failed to load title!\n" );
	    }
	    else{  
		//Main loop
		bool quit = false;
	   
		//Event handler
		SDL_Event e;
	   
		//While application is running
		while( !quit ) {
		    //Handle events on queue
		    while( SDL_PollEvent( &e ) != 0 ) {
		        if( e.type == SDL_KEYUP ) {//press SPACEBAR to close GameOverScreen and run LevelOne() again
		            if (e.key.keysym.sym == SDLK_SPACE){
		                quit = true;
		                closeGame();
		            }
		        }
		        //User requests quit
		        if( e.type == SDL_QUIT ) {
		            quit = true;
		        }
		    }
		    //Clear screen
		    SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
		    SDL_RenderClear( gRenderer );
	       
		    //Render
		    gameoverScreen.render( 0, 0 );

		    //Update screen
		    SDL_RenderPresent( gRenderer );
		}
	    }
	}
    //Free resources and close SDL
    closeGame();
}

void Play::closeGame(){
    titleScreen.free();
    background.free();
    gameoverScreen.free();

    //Free loaded images
    gTextTexture.free();
 
    //Free global font
    TTF_CloseFont( gFont );
    //gFont = NULL;
 
    //Destroy window   
    SDL_DestroyRenderer( gRenderer );
    SDL_DestroyWindow( gWindow );
    //gWindow.free  = NULL;
    //gRenderer = NULL;
 
    //Quit SDL subsystems
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}
 
 
/*void Play::closeGame() {
    titleScreen.free();
    background.free();
    gameoverScreen.free();
    SDL_DestroyRenderer( gRenderer );
    SDL_DestroyWindow( gWindow );
    gWindow = NULL;
    gRenderer = NULL;
    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}*/
