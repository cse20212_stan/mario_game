// Goomba.h
// enemy class for goomba
#ifndef GOOMBA_H
#define GOOMBA_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <iostream>
#include "Constants.h"
#include "LTexture.h"

using namespace std;
class Goomba : public Constants {
	public:
		Goomba(int, int, int, int);
		void move(SDL_Rect character, int); // Moves goomba without the collision detection	
		void Grender( SDL_Rect* clip = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE); // shows goomba on the screen
		void renderCurrent(int);
		void go(int);
		
		// gets the position
		int getX();
		int getY();
		
		// Change the animation value
		int getAnimation();
		void setAnimation(int);
		
		// Change flip value
		bool getFlip();
		void setFlip(bool);
		
		// gets the velocity of goomba
		int getVelX();
		int getVelY();	
		SDL_Rect getCollider();	
 
		// Collision Detection
		bool checkCollision(SDL_Rect , SDL_Rect );
		
		// SDL
		bool loadGoomba();
		void close();
 
		// animation
		SDL_Rect gSpriteClips[ GOOMBA_ANIMATION_FRAMES ];
		LTexture gSpriteSheetTexture;
		LTexture gGoombaTexture; // instance of an LTexture for a goomba
		
		bool getDead();
		void setDead(bool);

	private:
		int gPosX, gPosY; // x and y offsets of goomba
		double gVelX, gVelY;
		int minX, maxX;	// limits on motion
		
		SDL_Rect gCollider; // goomba's Collision Box;

		// Animation
		int animation;
		bool right, left, up, down;
		SDL_RendererFlip flipType = SDL_FLIP_NONE;
		bool flip;
		// Jumping
		int maxJump;
		bool dead;
};

// constructor for the goomba
Goomba::Goomba(int xpos, int ypos, int min, int max){
	// Initialize the offsets
	gPosX = xpos;
	gPosY = ypos; // gets the goomba on the ground
	
	minX = min;
	maxX = max;
	
	// Set Collision Box Dimensions
	gCollider.w = Goomba_WIDTH;
	gCollider.h = Goomba_HEIGHT;

	// initialize the velocity
	gVelX = 0;
	gVelY = 0;
	
	// Image Variables
	animation = 1;
	right = 1;
	left = 0;
	flipType = SDL_FLIP_NONE;
	
	dead = false;
}

void Goomba::go(int scroll){
	if(left){
		flip = 0;
		gVelX = -Goomba_VEL + scroll; 
		animation = 1;
	}
	else if(right){
		flip = 0;
		gVelX = Goomba_VEL + scroll;
		animation = 1;
	}
}

void Goomba::move(SDL_Rect character, int scroll){
	//Move the goomba left or right
	gPosX += gVelX;
	gCollider.x = gPosX;
	minX += scroll;
	maxX += scroll;
	
	//If the goomba went too far to the left or right
	if( gPosX <= minX || checkCollision(gCollider, character) )
	{
		// Change direction
		right = 1;
		left = 0;
	}
	else if( gPosX >= maxX || checkCollision(gCollider, character) )
	{
		// Change direction
		right = 0;
		left = 1;
	}

	//Move the goomba up or down
	gPosY += gVelY;
	gCollider.y = gPosY;

	// makes goomba move on the ground
	if( ( gPosY > 120 ) )//|| ( gPosY + Goomba_HEIGHT > SCREEN_HEIGHT ) )
	{
		//Move back
		gPosY -= gVelY;
	}
}

bool Goomba::checkCollision( SDL_Rect a, SDL_Rect b )
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	// If there is no collision
	if( bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB )
		return false;

	// If Mario kills goomba
	//else if( bottomA >= topB && topA <= bottomB )
		//return 1;
	
	// If goomba kills Mario
	else
		return true;
}

void Goomba::Grender(SDL_Rect* clip, SDL_RendererFlip flip){
	gGoombaTexture.render( getX(), getY(), clip, NULL, NULL, flip );
}

int Goomba::getX(){
	return gPosX;
}

int Goomba::getY(){
	return gPosY;
}

int Goomba::getAnimation(){
	return animation;
}

void Goomba::setAnimation(int val){
	animation = val;
}

bool Goomba::getFlip(){
	return flip;
}

void Goomba::setFlip(bool val){
	flip = val;
}

bool Goomba::loadGoomba(){
	//Loading success flag
	bool success = true;
	
	//Load Texture for goomba
	if( !gGoombaTexture.loadFromFile( "M-goomba.png","cyan" ) )
	{
		printf( "Failed to load goomba texture!\n" );
		success = false;
	}
	else
	{
		//Set sprite clips
		// left foot down
		gSpriteClips[ 0 ].x = 15;
		gSpriteClips[ 0 ].y = 4;
		gSpriteClips[ 0 ].w = 16;
		gSpriteClips[ 0 ].h = 16;
		
		gSpriteClips[ 1 ].x = 15;
		gSpriteClips[ 1 ].y = 4;
		gSpriteClips[ 1 ].w = 16;
		gSpriteClips[ 1 ].h = 16;
		
		// right foot down
		gSpriteClips[ 2 ].x = 65;
		gSpriteClips[ 2 ].y = 4;
		gSpriteClips[ 2 ].w = 16;
		gSpriteClips[ 2 ].h = 16;
		
		gSpriteClips[ 3 ].x = 65;
		gSpriteClips[ 3 ].y = 4;
		gSpriteClips[ 3 ].w = 16;
		gSpriteClips[ 3 ].h = 16;
		
		// smashed goomba
		gSpriteClips[ 4 ].x = 315;
		gSpriteClips[ 4 ].y = 11;
		gSpriteClips[ 4 ].w = 16;
		gSpriteClips[ 4 ].h = 9;
	}
}

void Goomba::renderCurrent(int frame){
				SDL_RendererFlip flipType = SDL_FLIP_NONE; 

				if(getFlip())
					flipType = SDL_FLIP_HORIZONTAL;
				else
					flipType = SDL_FLIP_NONE;

				// Render current frame
				SDL_Rect* currentClip = &gSpriteClips[ 0 ];
				switch(getAnimation()){
					case 1:
						currentClip = &gSpriteClips[ frame % 4 ];
						break;
					case 2:
						currentClip = &gSpriteClips[ 4 ];
						break;
				}

				Grender( currentClip, flipType); // render goomba
}

void Goomba::close(){
	gGoombaTexture.free();
}

SDL_Rect Goomba::getCollider()
{
	//Render wall
	return gCollider;
}

void Goomba::setDead(bool val){
	dead = val;
}

bool Goomba::getDead(void){
	return dead;
}

#endif
