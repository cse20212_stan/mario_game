// MysteryBox.h
// class for MysteryBox
#ifndef MYSTER_BOX_H
#define MYSTER_BOX_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <iostream>
#include "Constants.h"
#include "LTexture.h"

using namespace std;
class MysteryBox : public Constants {
	public:
		MysteryBox(int, int);
		bool move(int, SDL_Rect character); // Moves MysteryBox without the collision detection	
		void Mrender( SDL_Rect* clip = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE); // shows MysteryBox on the screen
		void renderCurrent(int);
		
		// gets the position
		int getX();
		int getY();
		
		bool getSmashed();
		void setSmashed(bool);
		
		SDL_Rect getCollider();	
 
		// Collision Detection
		bool checkGroundCollision(SDL_Rect , SDL_Rect );
		
		// SDL
		bool loadMysteryBox();
		void close();
		
		void move(int);
 
		// animation
		SDL_Rect gSpriteClips[ MYSTERY_BOX_ANIMATION_FRAMES ];
		LTexture gSpriteSheetTexture;
		LTexture gMysteryBoxTexture; // instance of an LTexture for a MysteryBox

	private:
		int bPosX, bPosY; // x and y offsets of MysteryBox
		double gVelX, gVelY;
		int minX, maxX;	// limits on motion
		
		SDL_Rect bCollider; // MysteryBox's Collision Box;

		// Animation
		int animation;
		bool right, left, up, down;
		SDL_RendererFlip flipType = SDL_FLIP_NONE;
		bool flip;
		// Jumping
		int maxJump;
		bool smashed;
};

// constructor for the MysteryBox
MysteryBox::MysteryBox(int xpos, int ypos){
	// Initialize the offsets
	bPosX = xpos;
	bPosY = ypos; // gets the MysteryBox on the ground

	// Set Collision Box Dimensions
	bCollider.x = bPosX - 1;
	bCollider.y = bPosY - 1;
	bCollider.w = MysteryBox_WIDTH + 2;
	bCollider.h = MysteryBox_HEIGHT + 2;

	// initialize the velocity
	gVelX = 0;
	gVelY = 0;

	// Image Variables
	animation = 1;
	right = 1;
	left = 0;
	flipType = SDL_FLIP_NONE;

	smashed = false;
}

bool MysteryBox::move(int scroll, SDL_Rect mario){
	bPosX += scroll;
	bCollider.x = bPosX - 1;
	if(checkGroundCollision(bCollider, mario))
		setSmashed(true);
}

bool MysteryBox::checkGroundCollision( SDL_Rect a, SDL_Rect b )
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	int overlap1, overlap2;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	// If there is no collision
	if( bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB )
		return false;

	// If Mario collides from below
	if( bottomA >= topB && topA <= topB )
		overlap1 = max(0, min(rightA, rightB) - max(leftA, leftB));
	else	// Mario collided with side
		overlap1 = 0;
	overlap2 = max(0, min(bottomA, bottomB) - max(topA, topB));
	
	if( overlap1 >= overlap2 )
		return true;
	else
		return false;
}

void MysteryBox::Mrender(SDL_Rect* clip, SDL_RendererFlip flip){
	gMysteryBoxTexture.render( getX(), getY(), clip, NULL, NULL, flip );
}

int MysteryBox::getX(){
	return bPosX;
}

int MysteryBox::getY(){
	return bPosY;
}

bool MysteryBox::loadMysteryBox(){
	//Loading success flag
	bool success = true;
	
	//Load Texture for MysteryBox
	if( !gMysteryBoxTexture.loadFromFile( "misc.png","cyan" ) )
	{
		printf( "Failed to load MysteryBox texture!\n" );
		success = false;
	}
	else
	{
		//Set sprite clips
		// left foot down
		gSpriteClips[ 0 ].x = 0;
		gSpriteClips[ 0 ].y = 64;
		gSpriteClips[ 0 ].w = 16;
		gSpriteClips[ 0 ].h = 16;
		
		gSpriteClips[ 1 ].x = 16;
		gSpriteClips[ 1 ].y = 64;
		gSpriteClips[ 1 ].w = 16;
		gSpriteClips[ 1 ].h = 16;

		gSpriteClips[ 2 ].x = 32;
		gSpriteClips[ 2 ].y = 64;
		gSpriteClips[ 2 ].w = 16;
		gSpriteClips[ 2 ].h = 16;
		
		gSpriteClips[ 3 ].x = 48;
		gSpriteClips[ 3 ].y = 64;
		gSpriteClips[ 3 ].w = 16;
		gSpriteClips[ 3 ].h = 16;
		
		// smashed block
		gSpriteClips[ 4 ].x = 64;
		gSpriteClips[ 4 ].y = 64;
		gSpriteClips[ 4 ].w = 16;
		gSpriteClips[ 4 ].h = 16;
	}
}

void MysteryBox::renderCurrent(int frame){
				SDL_RendererFlip flipType = SDL_FLIP_NONE;

				// Render current frame
				SDL_Rect* currentClip = &gSpriteClips[ 0 ];
				switch(smashed){
					case false:
						currentClip = &gSpriteClips[ frame % 4 ];
						break;
					case true:
						currentClip = &gSpriteClips[ 4 ];
						break;
				}

				Mrender( currentClip, flipType); // render MysteryBox
}

void MysteryBox::close(){
	gMysteryBoxTexture.free();
}

SDL_Rect MysteryBox::getCollider()
{
	//Render wall
	return bCollider;
}

void MysteryBox::setSmashed(bool val){
	smashed = val;
}

bool MysteryBox::getSmashed(void){
	return smashed;
}

#endif
