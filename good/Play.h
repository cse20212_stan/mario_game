// Play.h
// Program where the game is played
#ifndef PLAY_H
#define PLAY_H
//
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <iostream>
#include "LTexture.h"
#include "Constants.h"
#include "Mario.h"
#include "Wall.h"
#include "Goomba.h"
#include "MysteryBox.h"
#include "Mushroom.h"

using namespace std;

class Play : public Constants {
	public:
		// constructor
		Play(int);

		// Deconstructor
		~Play();

		//Starts up SDL and creates window
		bool init(int);

		//Loads media
		bool loadMediaOne();
		bool loadMediaTwo(string, string, string);

		// Play main while loop
		void LevelOne();
		void LevelTwo();

		//Frees media and shuts down SDL
		void closeGame();

		//The window we'll be rendering to
		SDL_Window* gWindow;


		// Title Screen
		LTexture titleScreen;
		bool initTitle();
		bool loadTitle(); // load the picture
		void TitleScreen(); // driver function of the title screen

		// GameOver Screen
		LTexture gameoverScreen;
		bool initGameOver();
		bool loadGameOver(); // load the gameover png
		void GameOverScreen(); // driver function of game over

		// Background
		LTexture background;
		LTexture background1;
		LTexture background2;
		LTexture background3;

		int checkCollision(SDL_Rect, SDL_Rect);
		int getLives();
		bool getQuit();
		bool getWin1();
		void setWin1(bool);
		bool getWin2();
		void setWin2(bool);

	private:
		int defaultLives;
		int lives;
		bool QUIT;
		bool win1;
		bool win2;
};

#endif



Play::Play(int val){
		//The window we'll be rendering to
		SDL_Window* gWindow = NULL;

		//Walking Animation?
		
		// Background
		LTexture background1;
		LTexture background2;
		LTexture background3;
		
		defaultLives = val;
		lives = val;
		QUIT = false;
		win1 = false;
		win2 = false;
}

Play::~Play(){
}

// Title Screen
//TITLE SCREEN
bool Play::initTitle(){
    //Initialization flag
    bool success = true;
 
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else {
        //Set texture filtering to linear
        if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ) {
            printf( "Warning: Linear texture filtering not enabled!" );
        }
 
        //Create window
        gWindow = SDL_CreateWindow( "STAN's World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, TITLESCREEN_WIDTH, TITLESCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL ) {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else {
            //Create vsynced renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
            if( gRenderer == NULL ) {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else {
                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
 
                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }
            }
        }
    }
    return success;
}
 
 
bool Play::loadTitle(){
//Loading success flag
    bool success = true;
    //Load background texture
    if( !titleScreen.loadFromFile( "TitleScreen.png","white" ) ) {
        printf( "Failed to load background texture!\n" );
        success = false;
    }
    return success;
}
 
void Play::TitleScreen() {
    //Start up SDL and create window
    if( !initTitle() ) {
        printf( "Failed to initialize!\n" );
    }
    else{
        //Load Title Screen
        if( !loadTitle() ) {
            printf( "Failed to load title!\n" );
        }
        else{  
            //Main loop
            bool quit = false;
           
            //Event handler
            SDL_Event e;
           
            //While application is running
            while( !quit ) {
                //Handle events on queue
                while( SDL_PollEvent( &e ) != 0 ) {
                    if( e.type == SDL_KEYUP ) {//press UP ARROW to close TitleScreen and run LevelOne()
                        if(e.key.keysym.sym == SDLK_SPACE){
                            quit = true;
                            closeGame();
                        }
                    }
                    //User requests quit
                    if( e.type == SDL_QUIT ) {
                        quit = true;
                        QUIT = true;
                    }
                }
               
                //Clear screen
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
                SDL_RenderClear( gRenderer );
               
                //Render
                titleScreen.render( 0, 0 );
 
                //Update screen
                SDL_RenderPresent( gRenderer );
            }
        }
    }
    //Free resources and close SDL
    closeGame();
}


bool Play::init(int level){
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		switch(level){
			case 1:
				gWindow = SDL_CreateWindow( "STAN's World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, LEVEL1_WIDTH, LEVEL1_HEIGHT, SDL_WINDOW_SHOWN );
				break;
			case 2:
				gWindow = SDL_CreateWindow( "STAN's World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, LEVEL2_WIDTH, LEVEL2_HEIGHT, SDL_WINDOW_SHOWN );
				break;
		}	
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool Play::loadMediaOne()
{
   
    //Loading success flag
    bool success = true;
 
    // Load Sprite Clips Here?
    if( TTF_Init() == -1 ) { 
        printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() ); 
        success = false; 
    }
 
    //Open the font
    gFont = TTF_OpenFont( "./SuperMario256.ttf", 20 );
    if( gFont == NULL )
    {
        printf( "Failed to load Mario font! SDL_ttf Error: %s\n", TTF_GetError() );
        success = false;
    }
    else
    {
        //Render text
        SDL_Color textColor = { 0, 256, 0 };
        if( !gTextTexture.loadFromRenderedText( "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMARIO\t\t\t\t\t\t\t\t\t\t\tWORLD\t\t\t\t\t\t\t\t TIME", textColor ) )
        {
            printf( "Failed to render text texture!\n" );
            success = false;
        }
    }  
   
    //Load background texture
    if( !background.loadFromFile( "background2.png","white" ) ){
        printf( "Failed to load background texture!\n" );
        success = false;
    }
   
    return success;
}

void Play::LevelOne(){
	// initialize all of the variables for mario
	//Start up SDL and create window
	if( !init(1) ){
		printf( "Failed to initialize!\n" );
	}
	else{
		//Load media
		if( !loadMediaOne() ){
			printf( "Failed to load media!\n" );
		}
		else{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			// Instantiation of Different Objects goes after this
			Mario mario(LEVEL1_WIDTH, LEVEL2_HEIGHT, 1);	
			mario.setWin(false);
			
			// Instantiation of goombas
			Goomba goomba(500,144, 464, 534);
			Goomba goomba2(760,144, 690, 825);
			Goomba goomba3(1100,144, 1040, 1140);
			Goomba goomba4(1650,144, 1600, 1715);
			
			MysteryBox box(500, 100);
			Mushroom shroom(500, 84);
			
			vector<Goomba> goombasVec {
				goomba, goomba2, goomba3, goomba4
			};
			
			mario.loadMario();
			goomba.loadGoomba();			
			goomba2.loadGoomba();
			goomba3.loadGoomba();
			goomba4.loadGoomba();
			box.loadMysteryBox();
			shroom.loadMushroom();	

			//The background scrolling offset
			int scrollingOffset = 0;

			//Current animation frame
			int frame = 0;
			int gframe = 0;
			int bframe = 0;
			
			//Set the wall
			Wall wall1(0, 162, 666, 50);
			Wall wall2(685, 162, 146, 50);
			Wall wall3(859, 162, 619, 50);
			Wall wall4(154,123,10,10);
			Wall wall5(212,83,10,10);
			Wall wall6(192,122,10,10);
			Wall wall7(203,122,10,10);
			Wall wall8(212,122,10,10);
			Wall wall9(223,122,10,10);
			Wall wall10(230,122,10,10);
			Wall wall11(270,141,20,20);
			Wall wall12(367,131, 20,30);
			Wall wall13(443,121,20,40);
			Wall wall14(549,121,20,40);
			Wall wall15(743,121,10,10);
			Wall wall16(753,121,10,10);
			Wall wall17(762,121,10,10);
			Wall wall18(771,83,80,10);
			Wall wall19(878,83,39,10);
			Wall wall20(908,83,10,10);
			Wall wall21(908,121,10,10);
			Wall wall22(965,121,20,10);
			Wall wall23(1023,121,10,10);
			Wall wall24(1052,121,10,10);
			Wall wall25(1081,121,10,10);
			Wall wall26(1052,83,10,10);
			Wall wall27(1139,121,10,10);
			Wall wall28(1168,83,30,10);
			Wall wall29(1245,121,20,10);
			Wall wall30(1235,83,10,10);
			Wall wall31(1245,83,10,10);
			Wall wall32(1255,83,10,10);
			Wall wall33(1265, 83,10, 10);
			Wall wall34(1294,150,40,10);
			Wall wall35(1303,141,30,10);
			Wall wall36(1313,130,20,10);
			Wall wall37(1322,121,10,10);
			Wall wall38(1352,121,10,10);
			Wall wall39(1352,131,20,10);
			Wall wall40(1352,141,30,10);
			Wall wall41(1352,150,40,10);
			Wall wall42(1429,150,50,10);
			Wall wall43(1438,140,40,10);
			Wall wall44(1448,131,30,10);
			Wall wall45(1457,121,20,10);
			Wall wall46(1496,121,10,10);
			Wall wall47(1496,131,20,10);
			Wall wall48(1496,151,40,10);
			Wall wall49(1573,140,21,20);
			Wall wall50(1622,121,20,10);
			Wall wall51(1642,121,10,10);
			Wall wall52(1650,120,10,10);
			Wall wall53(1728,140,20,20);
			Wall wall54(1749,150,90,10);
			Wall wall55(1757,140,80,10);
			Wall wall56(1767,131,70,10);
			Wall wall57(1777,121,60,10);
			Wall wall58(1786,112,50,10);
			Wall wall59(1796,101,40,10);
			Wall wall60(1805,92,30,10);
			Wall wall70(1814,82,20,10);
			Wall wall71(1496,162,1000,42);
			Wall wall72(1496,141,28,10);
			Wall wall73(498, 100, 16, 16);
			
			// Create the vector of boxes
			vector<Wall> wallsVec {
				wall1,
				wall2,
				wall3,
				wall4,
				wall5,
				wall6,
				wall7,
				wall8,
				wall9,
				wall10,
				wall11,
				wall12,
				wall13,
				wall14,
				wall15,
				wall16,
				wall17,
				wall18,
				wall19,
				wall20,
				wall21,
				wall22,
				wall23,
				wall24,
				wall25,
				wall26,
				wall27,
				wall28,
				wall29,
				wall30,
				wall31,
				wall32,
				wall33,
				wall34,
				wall35,
				wall36,
				wall37,
				wall38,
				wall39,
				wall40,
				wall41,
				wall42,
				wall43,
				wall44,
				wall45,
				wall46,
				wall47,
				wall48,
				wall49,
				wall50,
				wall51,
				wall52,
				wall53,
				wall54,
				wall55,
				wall56,
				wall57,
				wall58,
				wall59,
				wall60,
				wall70,
				wall71, 
				wall72,
				wall73
			};

			//SDL_RendererFlip flipType = SDL_FLIP_NONE; 

			//While application is running
			while( !quit && mario.getDead() < 2 )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
						QUIT = true;
					}

					//Handle input for the dot
					mario.handleEvent( e );
				}
				goomba.go(-mario.getdx());
				goomba2.go(-mario.getdx());
				goomba3.go(-mario.getdx());
				goomba4.go(-mario.getdx());	

				//Move the Objects
				//SDL_Rect obstacle = wall.getCollider();
				mario.move( wallsVec, scrollingOffset, scrollingOffset );		// Pass in vector of objects to check collisions
				
				SDL_Rect mCollider = mario.getCollider();
				goomba.move( mCollider, -mario.getdx() );
				goomba2.move( mCollider, -mario.getdx() );
				goomba3.move( mCollider, -mario.getdx() );
				goomba4.move( mCollider, -mario.getdx() );
				for(int i = 0; i < wallsVec.size(); i++)
					wallsVec[i].move(-mario.getdx());
					
				if(box.move(-mario.getdx(), mCollider))
					shroom.setActive(true);
				shroom.move(-mario.getdx(), wallsVec);
				SDL_Rect sCollider = shroom.getCollider();
				if(checkCollision(mCollider, sCollider)){
					shroom.setActive(false);
					if(mario.getBig())
						lives++;
					else
						mario.setBig(true);
				}

				//Scroll background
				scrollingOffset -= mario.getdx();

				//Clear screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );
				
				//Render background
				background.render( scrollingOffset, 0 );
				background.render( scrollingOffset + background.getWidth(), 0 );
				
				//***************************************************************
				//*********************Draw Debugging Boxes**********************
				//***************************************************************
				/*for(int i = 0; i < wallsVec.size(); i++)
					wallsVec[i].render();
				SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );		
				SDL_RenderDrawRect( gRenderer, &mCollider );*/
				//***************************************************************
					
				if( !goomba.getDead() && checkCollision(mario.getCollider(), goomba.getCollider()) == 1)
					goomba.setDead(true);
				else if( !goomba.getDead() && checkCollision(mario.getCollider(), goomba.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}

					
				if( !goomba2.getDead() && checkCollision(mario.getCollider(), goomba2.getCollider()) == 1)
					goomba2.setDead(true);
				else if( !goomba2.getDead() && checkCollision(mario.getCollider(), goomba2.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}
					
				if( !goomba3.getDead() && checkCollision(mario.getCollider(), goomba3.getCollider()) == 1)
					goomba3.setDead(true);
				else if( !goomba3.getDead() && checkCollision(mario.getCollider(), goomba3.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}
					
				if( !goomba4.getDead() && checkCollision(mario.getCollider(), goomba4.getCollider()) == 1)
					goomba4.setDead(true);
				else if( !goomba4.getDead() && checkCollision(mario.getCollider(), goomba4.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}
				
				if(mario.getX() >= LEVEL1_WIDTH - 145){
					win1 = true;
					mario.setVictory(true);
				}
				if(mario.getWin()){
					mario.setVictory(false);  // resets for next level
					quit = true;
				}
				
				if(mario.getDead() != 2)
					mario.renderCurrent(frame);
				else {
					lives--;
					quit = true;
				}

				if(!goomba.getDead())
					goomba.renderCurrent(gframe);
				if(!goomba2.getDead())
					goomba2.renderCurrent(gframe);
				if(!goomba3.getDead())
					goomba3.renderCurrent(gframe);
				if(!goomba4.getDead())
					goomba4.renderCurrent(gframe);
				box.renderCurrent(bframe);
				shroom.renderCurrent(0);

//Render current frame
				gTextTexture.render( ( 0 ) , ( 0 )  );

				//Update screen
				SDL_RenderPresent( gRenderer );

				// go to the next frame
				++frame;
				++gframe;
				++bframe;
				
				// Cycle animation
				if( frame/4 >= 4){
					frame = 0;
				}
				if( gframe/2 >=GOOMBA_ANIMATION_FRAMES ){
					gframe = 0;
				}
				if( bframe/4 >= MYSTERY_BOX_ANIMATION_FRAMES ){
					bframe = 0;
				}
			}
		}
	}
	//Free resources and close SDL
	closeGame();
}

bool Play::loadMediaTwo(string FILE1, string FILE2, string FILE3)
{
   
    //Loading success flag
    bool success = true;
 
    // Load Sprite Clips Here?
    if( TTF_Init() == -1 ) { 
        printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() ); 
        success = false; 
    }
 
    //Open the font
    gFont = TTF_OpenFont( "./SuperMario256.ttf", 20 );
    if( gFont == NULL )
    {
        printf( "Failed to load Mario font! SDL_ttf Error: %s\n", TTF_GetError() );
        success = false;
    }
    else
    {
        //Render text
        SDL_Color textColor = { 0, 256, 0 };
        if( !gTextTexture.loadFromRenderedText( "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMARIO\t\t\t\t\t\t\t\t\t\t\tWORLD\t\t\t\t\t\t\t\t TIME", textColor ) )
        {
            printf( "Failed to render text texture!\n" );
            success = false;
        }
    } 
   
    //Load background texture
    if( !background1.loadFromFile( FILE1,"white" ) ){
        printf( "Failed to load background texture!\n" );
        success = false;
    }
    if( !background2.loadFromFile( FILE2,"white" ) ){
        printf( "Failed to load background texture!\n" );
        success = false;
    }
    if( !background3.loadFromFile( FILE3,"white" ) ){
        printf( "Failed to load background texture!\n" );
        success = false;
    }
   
    return success;
}

void Play::LevelTwo(){
	// initialize all of the variables for mario
	//Start up SDL and create window
	if( !init(2) ){
		printf( "Failed to initialize!\n" );
	}
	else{
		//Load media
		if( !loadMediaTwo("level1.bmp", "level2.bmp", "level3.bmp") ){
			printf( "Failed to load media!\n" );
		}
		else{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			// Instantiation of Different Objects goes after this
			Mario mario(LEVEL2_WIDTH, LEVEL2_HEIGHT, 2);	
			mario.setWin(false);
			
			// Instantiation of goombas
			Goomba goomba(600,414, 524, 857);
			Goomba goomba2(800,414, 500, 1000);
			Goomba goomba3(1200,414, 1000, 1300);
			
			MysteryBox box1(114, 280);
			Mushroom shroom1(114, 264);
         MysteryBox box2(2108,363);
			Mushroom shroom2(2108, 347);
			
			vector<Goomba> goombasVec {
				goomba, goomba2, goomba3
			};
			
			mario.loadMario();
			goomba.loadGoomba();
			box1.loadMysteryBox();
			shroom1.loadMushroom();
			box2.loadMysteryBox();
			shroom2.loadMushroom();

			//The background scrolling offset
			int scrollingOffset1 = 0;
			int scrollingOffset2 = 2048;
			int scrollingOffset3 = 4096;

			//Current animation frame
			int frame = 0;
			int gframe = 0;
			int bframe = 0;
			
			//Set the wall
			Wall wall1(0, 431, 1077, 74);
         Wall wall2(292, 399, 63, 32);
         Wall wall3(461, 399, 32, 32);
         Wall wall4(325,380,31,20);
         Wall wall5(494,367,31,64);
         Wall wall6(83,332,77,13);
         Wall wall7(617,308,85,13);
         Wall wall8(874,405,42,25);
         Wall wall9(1078,399,159,105);
         Wall wall10(1110,380,127,21);
         Wall wall11(1236,401,86,103);
         Wall wall12(1322,380,70,124);
         Wall wall13(1242,297,78,13);
         Wall wall14(1258,214,30,13);
         Wall wall15(1502,308,69,13);
         Wall wall16(1676,218,80,15);
         Wall wall17(1930,312,63,108);
         Wall wall18(2093,244,76,11);
         Wall wall19(2355,370,52,57);
         Wall wall20(2526,324,59,10);
         Wall wall21(2758,386,74,42);
         Wall wall22(2792,352,26,28);
         Wall wall23(2839,402,11,24);
         Wall wall24(2855,416,10,10);
         Wall wall25(2779,206,64,10);
         Wall wall26(3121,400,30,28);
         Wall wall27(3155,350,136,150);
         Wall wall28(2347,434,824,66);
         Wall wall29(3465,257,64,10);
         Wall wall30(3702,131,64,10);
         Wall wall31(3892,293,64,10);
         Wall wall32(4142,193,64,10);
         Wall wall33(4423,262,98,153);
         Wall wall34(4673,262,134,42);
         Wall wall35(4721,312,41,103);
         Wall wall36(4961,262,106,153);
         Wall wall37(5219,262,126,153);
         Wall wall38(5527,436,1625,64);
         Wall wall39(5527,370,126,130);
         Wall wall40(2261,316,85,112);
         Wall wall41(1930,418,414,82);
         Wall wall42(2038,316,173,23);
         Wall wall43(2038,316,39,48);
         Wall wall44(2187,316,23,48);
         Wall wall45(2108,363,10,10); //Mystery Box
			
			// Create the vector of boxes
			vector<Wall> wallsVec {
				wall1,
				wall2,
				wall3,
				wall4,
				wall5,
				wall6,
				wall7,
				wall8,
				wall9,
				wall10,
				wall11,
				wall12,
				wall13,
				wall14,
				wall15,
				wall16,
				wall17,
				wall18,
				wall19,
				wall20,
				wall21,
				wall22,
				wall23,
				wall24,
				wall25,
				wall26,
				wall27,
				wall28,
				wall29,
				wall30,
				wall31,
				wall32,
				wall33,
				wall34,
				wall35,
				wall36,
				wall37,
				wall38,
				wall39,
				wall40,
				wall41,
				wall42,
				wall43,
				wall44,
				wall45
			};

			//SDL_RendererFlip flipType = SDL_FLIP_NONE; 

			//While application is running
			while( !quit && mario.getDead() < 2 )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
						QUIT = true;
					}

					//Handle input for the dot
					mario.handleEvent( e );
				}
				//for(int i = 0; i < goombasVec.size(); i++)
					//goombasVec[i].go(-mario.getdx());
				goomba.go(-mario.getdx());

				//Move the Objects
				//SDL_Rect obstacle = wall.getCollider();
				mario.move( wallsVec, scrollingOffset1, scrollingOffset3 );		// Pass in vector of objects to check collisions
				
				SDL_Rect mCollider = mario.getCollider();
				//for(int i = 0; i < goombasVec.size(); i++)
					//goombasVec[i].move( mCollider, -mario.getdx() );
				goomba.move(mCollider, -mario.getdx());
				for(int i = 0; i < wallsVec.size(); i++)
					wallsVec[i].move(-mario.getdx());
					
				if(box1.move(-mario.getdx(), mCollider))
					shroom1.setActive(true);
				shroom1.move(-mario.getdx(), wallsVec);
				SDL_Rect sCollider1 = shroom1.getCollider();
				if(checkCollision(mCollider, sCollider1)){
					shroom1.setActive(false);
					if(mario.getBig())
						lives++;
					else
						mario.setBig(true);
				}
				if(box2.move(-mario.getdx(), mCollider))
					shroom2.setActive(true);
				shroom2.move(-mario.getdx(), wallsVec);
				SDL_Rect sCollider2 = shroom2.getCollider();
				if(checkCollision(mCollider, sCollider2)){
					shroom2.setActive(false);
					if(mario.getBig())
						lives++;
					else
						mario.setBig(true);
				}

				//Scroll background
				scrollingOffset1 -= mario.getdx();
				scrollingOffset2 -= mario.getdx();
				scrollingOffset3 -= mario.getdx();

				//Clear screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );
				
				//Render background
				background1.render( scrollingOffset1, 0 );
				background2.render( scrollingOffset2, 0 );
				background3.render( scrollingOffset3, 0 );
				//background.render( scrollingOffset + background.getWidth(), 0 );
				
				//***************************************************************
				//*********************Draw Debugging Boxes**********************
				//***************************************************************
				/*for(int i = 0; i < wallsVec.size(); i++)
					wallsVec[i].render();
				SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );		
				SDL_RenderDrawRect( gRenderer, &mCollider );*/
				//***************************************************************
					
				if( !goomba.getDead() && checkCollision(mario.getCollider(), goomba.getCollider()) == 1)
					goomba.setDead(true);
				else if( !goomba.getDead() && checkCollision(mario.getCollider(), goomba.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}

					
				if( !goomba2.getDead() && checkCollision(mario.getCollider(), goomba2.getCollider()) == 1)
					goomba2.setDead(true);
				else if( !goomba2.getDead() && checkCollision(mario.getCollider(), goomba2.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}
					
				if( !goomba3.getDead() && checkCollision(mario.getCollider(), goomba3.getCollider()) == 1)
					goomba3.setDead(true);
				else if( !goomba3.getDead() && checkCollision(mario.getCollider(), goomba3.getCollider()) == 2 ){
					mario.setDead(1);
					mario.setMaxJump( mario.getY() - 50 );
				}
				
				if(mario.getX() >= LEVEL2_WIDTH - 227){
					win2 = true;
					mario.setVictory(true);
				}
				if(mario.getWin()){
					mario.setVictory(false);  // resets for next level
					quit = true;
				}

				// Render if alive, decrement lives and quit if dead
				if(mario.getDead() != 2)
					mario.renderCurrent(frame);
				else { 
					lives--;
					cout << "Lives: " << mario.getDead() << endl;
					quit = true;
				}

				if(!goomba.getDead())
					goomba.renderCurrent(gframe);
				if(!goomba2.getDead())
					goomba2.renderCurrent(gframe);
				if(!goomba3.getDead())
					goomba3.renderCurrent(gframe);
					
				box1.renderCurrent(bframe);
				shroom1.renderCurrent(0);
				box2.renderCurrent(bframe);
				shroom2.renderCurrent(0);


//Render current frame
				gTextTexture.render( ( 0 ) , ( 0 )  );

				//Update screen
				SDL_RenderPresent( gRenderer );

				// go to the next frame
				++frame;
				++gframe;
				++bframe;
				
				// Cycle animation
				if( frame/4 >= 4){
					frame = 0;
				}
				if(gframe/2 >=GOOMBA_ANIMATION_FRAMES){
					gframe = 0;
				}
				if( bframe/4 >= MYSTERY_BOX_ANIMATION_FRAMES ){
					bframe = 0;
				}
			}
		}
	}
	//Free resources and close SDL
	closeGame();
}

int Play::checkCollision( SDL_Rect a, SDL_Rect b )
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	int overlap1, overlap2;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	// If there is no collision
	if( bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB )
		return 0;

	// If Mario kills goomba
	if( bottomA >= topB && topA <= topB )
		overlap1 = max(0, min(rightA, rightB) - max(leftA, leftB));
	// If goomba kills Mario
	else
		overlap1 = 0;
	overlap2 = max(0, min(bottomA, bottomB) - max(topA, topB));
	
	if( overlap1 >= overlap2 )
		return 1;
	else
		return 2;
}

// GAMEOVER SCREEN
bool Play::initGameOver(){
    //Initialization flag
    bool success = true;
 
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else {
        //Set texture filtering to linear
        if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ) {
            printf( "Warning: Linear texture filtering not enabled!" );
        }
 
        //Create window
        gWindow = SDL_CreateWindow( "STAN's World", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, GAMEOVERSCREEN_WIDTH, GAMEOVERSCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL ) {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else {
            //Create vsynced renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
            if( gRenderer == NULL ) {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else {
                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
 
                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }
            }
        }
    }
    return success;
}
 
 
bool Play::loadGameOver(){
//Loading success flag
    bool success = true;
    //Load background texture
    if( !gameoverScreen.loadFromFile( "MarioGameOver.png","white" ) ) {
        printf( "Failed to load Game Over texture!\n" );
        success = false;
    }
    return success;
}
 
void Play::GameOverScreen() {
	//Start up SDL and create window
	if( !initGameOver() ) {
	    printf( "Failed to initialize!\n" );
	}
	else{
	    //Load Title Screen
	    if( !loadGameOver() ) {
		printf( "Failed to load title!\n" );
	    }
	    else{  
		//Main loop
		bool quit = false;
	   
		//Event handler
		SDL_Event e;
	   
		//While application is running
		while( !quit ) {
		    //Handle events on queue
		    while( SDL_PollEvent( &e ) != 0 ) {
		        if( e.type == SDL_KEYUP ) {//press SPACEBAR to close GameOverScreen and run LevelOne() again
		            if (e.key.keysym.sym == SDLK_SPACE){
		                quit = true;
		                closeGame();
		            }
		        }
		        //User requests quit
		        if( e.type == SDL_QUIT ) {
		            quit = true;
		        }
		    }
		    //Clear screen
		    SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
		    SDL_RenderClear( gRenderer );
	       
		    //Render
		    gameoverScreen.render( 0, 0 );

		    //Update screen
		    SDL_RenderPresent( gRenderer );
		}
	    }
	}
    //Free resources and close SDL
    closeGame();
}

int Play::getLives(){
	return lives;
}

bool Play::getQuit(){
	return QUIT;
}

bool Play::getWin1(){
	return win1;
}

void Play::setWin1(bool val){
	win1 = val;
}

bool Play::getWin2(){
	return win2;
}

void Play::setWin2(bool val){
	win2 = val;
}

void Play::closeGame(){
    titleScreen.free();
    background.free();
    gameoverScreen.free();

    //Free loaded images
    gTextTexture.free();
 
    //Free global font
    TTF_CloseFont( gFont );
    //gFont = NULL;
 
    //Destroy window   
    SDL_DestroyRenderer( gRenderer );
    SDL_DestroyWindow( gWindow );
    //gWindow.free  = NULL;
    //gRenderer = NULL;
 
    //Quit SDL subsystems
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}
 
 
/*void Play::closeGame() {
    titleScreen.free();
    background.free();
    gameoverScreen.free();
    SDL_DestroyRenderer( gRenderer );
    SDL_DestroyWindow( gWindow );
    gWindow = NULL;
    gRenderer = NULL;
    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}*/
