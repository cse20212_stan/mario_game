// Constants.h
// Base Class which holds all of the constants
// f
#ifndef CONSTANTS
#define CONSTANTS

#include <iostream>

using namespace std;

class Constants{

	protected:
		// Screen dimensions
		static const int LEVEL1_WIDTH = 700;
		static const int LEVEL1_HEIGHT = 203;
		static const int LEVEL2_WIDTH = 700;
		static const int LEVEL2_HEIGHT = 504;
		
		int SCROLL = 0;

		// Title Screen
		static const int TITLESCREEN_WIDTH = 636;
		static const int TITLESCREEN_HEIGHT = 357;

		// Game Over Screen
		static const int GAMEOVERSCREEN_WIDTH = 540;
		static const int GAMEOVERSCREEN_HEIGHT = 405;
		
		// Mario Constants
		static const int Mario_WIDTH = 15;
		static const int Mario_HEIGHT = 21;
		static const int Mario_DuckHeight = 15;
		static const int BigMario_WIDTH = 17;
		static const int BigMario_HEIGHT = 29;
		static const int BigMario_DuckHeight = 14;
		static const int Mario_VelX = 5;
		static const int Mario_VelY = 3;
		static const int JUMP = 100;
		// Mario Animation
		static const int MARIO_ANIMATION_FRAMES = 16;
		
		//Goomba Constants
		static const int Goomba_WIDTH = 16;
		static const int Goomba_HEIGHT = 16;
		static constexpr double Goomba_VEL = 1;
		// Goomba Animation
		static const int GOOMBA_ANIMATION_FRAMES = 5;
		
		static const int MysteryBox_WIDTH = 16;
		static const int MysteryBox_HEIGHT = 16;
		static const int MYSTERY_BOX_ANIMATION_FRAMES = 5;
		
		
		static const int Mushroom_WIDTH = 16;
		static const int Mushroom_HEIGHT = 16;
		static const int Mushroom_VEL = 2;
};
# endif 
