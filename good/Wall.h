// Wall.h
// This is the class for mario, should hold his position and load the sprite for mario
//
#ifndef WALL_H
#define WALL_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <iostream>
#include "Constants.h"
#include "LTexture.h"

using namespace std;
class Wall : public Constants {
	public:
		Wall(int, int, int, int);
		void move(int); // Moves Wall without the collision detection	
		void render(); // shows mario on the screen
		
		// gets the position
		int getX();
		int getY(); 
		SDL_Rect getCollider();

	private:
		int wPosX, wPosY; // x and y offsets of mario
		double wVelX, wVelY;
		
		SDL_Rect wCollider; // Wall's Collision Box
};

Wall::Wall(int x, int y, int width, int height){
	// Set Collision Box Dimensions
	wCollider.x = x;
	wCollider.y = y;
	wCollider.w = width;
	wCollider.h = height;

	// initialize the velocity
	wVelX = SCROLL;
	wVelY = 0;
}

void Wall::move(int scroll)
{
	 wVelX = scroll;
    //Move the dot left or right
    wCollider.x += wVelX;
}

void Wall::render()
{
	//Render wall
	SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );		
	SDL_RenderDrawRect( gRenderer, &wCollider );
}

SDL_Rect Wall::getCollider()
{
	//Render wall
	return wCollider;
}
#endif
