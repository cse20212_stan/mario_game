// Mushroom.h
// enemy class for mushroom
#ifndef MUSHROOM_H
#define MUSHROOM_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <iostream>
#include "Constants.h"
#include "LTexture.h"
#include "Wall.h"

using namespace std;
class Mushroom : public Constants {
	public:
		Mushroom(int, int);
		void move(int, vector<Wall> &); // Moves mushroom without the collision detection	
		void Mrender( SDL_Rect* clip = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE); // shows mushroom on the screen
		void renderCurrent(int);
		void go(int);
		
		// gets the position
		int getX();
		int getY();
		
		// Change the animation value
		int getAnimation();
		void setAnimation(int);
		
		// Change flip value
		bool getFlip();
		void setFlip(bool);
		
		// gets the velocity of mushroom
		int getVelX();
		int getVelY();	
		SDL_Rect getCollider();	
 
		// Collision Detection
		bool checkCollision(SDL_Rect, SDL_Rect);
		bool checkWallCollision(SDL_Rect, vector<Wall> &);
		
		// SDL
		bool loadMushroom();
		void close();
 
		// animation
		SDL_Rect gSpriteClips[ GOOMBA_ANIMATION_FRAMES ];
		LTexture gSpriteSheetTexture;
		LTexture gMushroomTexture; // instance of an LTexture for a mushroom
		
		bool getActive();
		void setActive(bool);
		bool getDead();
		void setDead(bool);

	private:
		int mPosX, mPosY; // x and y offsets of mushroom
		double mVelX, mVelY;
		
		SDL_Rect mCollider; // mushroom's Collision Box;

		// Animation
		int animation;
		bool right, left, up, down;
		SDL_RendererFlip flipType = SDL_FLIP_NONE;
		bool flip;
		// Jumping
		int maxJump;
		bool active;
		bool dead;
};

// constructor for the mushroom
Mushroom::Mushroom(int xpos, int ypos){
	// Initialize the offsets
	mPosX = xpos;
	mPosY = ypos; // gets the mushroom on the ground
	
	// Set Collision Box Dimensions
	mCollider.x = mPosX;
	mCollider.y = mPosY;
	mCollider.w = MysteryBox_WIDTH;
	mCollider.h = MysteryBox_HEIGHT;

	// initialize the velocity
	mVelX = Mushroom_VEL;
	mVelY = 2* Mushroom_VEL;
	
	// Image Variables
	animation = 1;
	right = 1;
	left = 0;
	flipType = SDL_FLIP_NONE;
	active = false;
	dead = false;
}

void Mushroom::move(int scroll, vector<Wall> & walls){
	if(active && !dead){
		//Move the mushroom left or right
		mPosX += mVelX + scroll;
		mCollider.x = mPosX;
	
		//If the mushroom went too far to the left or right
		if( checkWallCollision(mCollider, walls) )
		{
			// Change direction
			mVelX = -mVelX;
			mPosX += mVelX;
			mCollider.x = mPosX;
		}

		//Move the mushroom up or down
		mPosY += mVelY;
		mCollider.y = mPosY;

		// makes mushroom move on the ground
		if( checkWallCollision(mCollider, walls) )
		{
			//Move back
			mPosY -= mVelY;
			mCollider.y = mPosY;
		}
	}
	else{
		mPosX += scroll;
	}
}

bool Mushroom::checkCollision( SDL_Rect a, SDL_Rect b )
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	// If there is no collision
	if( bottomA <= topB || topA >= bottomB || rightA <= leftB || leftA >= rightB )
		return false;
	else
		return true;
}

bool Mushroom::checkWallCollision(SDL_Rect shroom, vector<Wall> & walls) {
	for(int i = 0; i < walls.size(); i++) {
		SDL_Rect obstacle = walls[i].getCollider();
		if(checkCollision(shroom, obstacle))
			return true;
	}
}

void Mushroom::Mrender(SDL_Rect* clip, SDL_RendererFlip flip){
	gMushroomTexture.render( getX(), getY(), clip, NULL, NULL, flip );
}

int Mushroom::getX(){
	return mPosX;
}

int Mushroom::getY(){
	return mPosY;
}

int Mushroom::getAnimation(){
	return animation;
}

void Mushroom::setAnimation(int val){
	animation = val;
}

bool Mushroom::getFlip(){
	return flip;
}

void Mushroom::setFlip(bool val){
	flip = val;
}

bool Mushroom::loadMushroom(){
	//Loading success flag
	bool success = true;
	
	//Load Texture for mushroom
	if( !gMushroomTexture.loadFromFile( "SuperMushroom.png","cyan" ) )
	{
		printf( "Failed to load mushroom texture!\n" );
		success = false;
	}
	else
	{
		//Set sprite clips
		// left foot down
		gSpriteClips[ 0 ].x = 0;
		gSpriteClips[ 0 ].y = 0;
		gSpriteClips[ 0 ].w = 16;
		gSpriteClips[ 0 ].h = 16;
	}
}

void Mushroom::renderCurrent(int frame){
	if(active && !dead){
		SDL_RendererFlip flipType = SDL_FLIP_NONE; 

		if(getFlip())
			flipType = SDL_FLIP_HORIZONTAL;
		else
			flipType = SDL_FLIP_NONE;

		// Render current frame
		SDL_Rect* currentClip = &gSpriteClips[ 0 ];

		Mrender( currentClip, flipType); // render mushroom
	}
}

void Mushroom::close(){
	gMushroomTexture.free();
}

SDL_Rect Mushroom::getCollider()
{
	//Render wall
	return mCollider;
}

void Mushroom::setActive(bool val){
	active = val;
}

bool Mushroom::getActive(void){
	return active;
}

void Mushroom::setDead(bool val){
	dead = val;
}

bool Mushroom::getDead(void){
	return dead;
}

#endif
