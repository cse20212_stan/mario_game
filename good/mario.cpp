// mario.cpp
// main driver program, a.out is the executable
//
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <iostream>
#include "Play.h"
#include "LTexture.h"

using namespace std;

int main(){
	// show the title screen
	int lives = 3;
	// Instantiation of a game
	Play mario(lives);

	// Running the game
	mario.TitleScreen(); // pressing the space bar closes the title screen and begins the game
	while( !mario.getQuit() && mario.getLives() && !mario.getWin1() )
		mario.LevelOne();
	while( !mario.getQuit() && mario.getLives() && !mario.getWin2())
		mario.LevelTwo();
	if(!mario.getQuit()) 	
		mario.GameOverScreen();
	return 0;
}
