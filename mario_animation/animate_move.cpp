/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Texture wrapper class
class LTexture
{
	public:
		//Initializes variables
		LTexture();

		//Deallocates memory
		~LTexture();

		//Loads image at specified path
		bool loadFromFile( std::string path );
		
		#ifdef _SDL_TTF_H
		//Creates image from font string
		bool loadFromRenderedText( std::string textureText, SDL_Color textColor );
		#endif

		//Deallocates texture
		void free();

		//Set color modulation
		void setColor( Uint8 red, Uint8 green, Uint8 blue );

		//Set blending
		void setBlendMode( SDL_BlendMode blending );

		//Set alpha modulation
		void setAlpha( Uint8 alpha );
		
		//Renders texture at given point
		void render( int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE );

		//Gets image dimensions
		int getWidth();
		int getHeight();

	private:
		//The actual hardware texture
		SDL_Texture* mTexture;

		//Image dimensions
		int mWidth;
		int mHeight;
};

//The Mario that will move around on the screen
class Mario
{
    public:
		//The dimensions of the Mario
		static const int Mario_WIDTH = 17;
		static const int Mario_HEIGHT = 29;

		//Maximum axis velocity of the Mario
		static const int Mario_VEL = 1;

		//Initializes the variables
		Mario();

		//Takes key presses and adjusts the Mario's velocity
		void handleEvent( SDL_Event& e );

		//Moves the Mario and checks collision
		void move( SDL_Rect& wall );

		//Shows the Mario on the screen
		void render();
		
		//Gets position
		int getX();
		int getY();
		
		//Change animation value
		int getAnimation();
		void setAnimation(int);
		
		//Change flip value
		bool getFlip();
		void setFlip(bool);

    private:
		//The X and Y offsets of the Mario
		int mPosX, mPosY;

		//The velocity of the Mario
		int mVelX, mVelY;
		
		//Mario's collision box
		SDL_Rect mCollider;
		
		//Which animation to display
		int animation;
		bool right, left, up, down;
		SDL_RendererFlip flipType = SDL_FLIP_NONE;
		bool flip;
		
		int maxJump;
};

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Box collision detector
bool checkCollision( SDL_Rect a, SDL_Rect b );

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Walking animation
const int WALKING_ANIMATION_FRAMES = 5;
SDL_Rect gSpriteClips[ WALKING_ANIMATION_FRAMES ];
LTexture gSpriteSheetTexture;

//Scene textures
LTexture gMarioTexture;

LTexture::LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture()
{
	//Deallocate
	free();
}

bool LTexture::loadFromFile( std::string path )
{
	//Get rid of preexisting texture
	free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Color key image
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

#ifdef _SDL_TTF_H
bool LTexture::loadFromRenderedText( std::string textureText, SDL_Color textColor )
{
	//Get rid of preexisting texture
	free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid( gFont, textureText.c_str(), textColor );
	if( textSurface != NULL )
	{
		//Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
		if( mTexture == NULL )
		{
			printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}
	else
	{
		printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
	}

	
	//Return success
	return mTexture != NULL;
}
#endif

void LTexture::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void LTexture::setColor( Uint8 red, Uint8 green, Uint8 blue )
{
	//Modulate texture rgb
	SDL_SetTextureColorMod( mTexture, red, green, blue );
}

void LTexture::setBlendMode( SDL_BlendMode blending )
{
	//Set blending function
	SDL_SetTextureBlendMode( mTexture, blending );
}
		
void LTexture::setAlpha( Uint8 alpha )
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod( mTexture, alpha );
}

void LTexture::render( int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip )
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if( clip != NULL )
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx( gRenderer, mTexture, clip, &renderQuad, angle, center, flip );
}

int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}

Mario::Mario()
{
	//Initialize the offsets
	mPosX = SCREEN_WIDTH / 2;
	mPosY = 0;

	//Set collision box dimension
	mCollider.w = Mario_WIDTH;
	mCollider.h = Mario_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;

	//Image variables
	animation = 0;
	right = 0;
	left = 0;
	up = 0;
	down = 0;
	flipType = SDL_FLIP_NONE;
	maxJump = 0;
}

void Mario::handleEvent( SDL_Event& e )
{
	//If a key was pressed
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 )
	{
		//Adjust the velocity
		switch( e.key.keysym.sym )
		{
			case SDLK_UP:
				up = 1;
				down = 0;
				maxJump = mPosY - 50;
				break;
			case SDLK_DOWN:
				down = 1;
				up = 0;
				mPosY = mPosY + 15;
				break;
			case SDLK_LEFT:
				left = 1;
				right = 0;
				break;
			case SDLK_RIGHT:
				right = 1;
				left = 0;
				break;
		}
	}
	//If a key was released
	else if( e.type == SDL_KEYUP && e.key.repeat == 0 )
	{
		//Adjust the velocity
		switch( e.key.keysym.sym )
		{
			case SDLK_UP:
				up = 0;
				break;
			case SDLK_DOWN: 
				down = 0;
				mPosY = mPosY - 15;
				break;
			case SDLK_LEFT:
				left = 0;
				break;
			case SDLK_RIGHT:
				right = 0;
				break;
		}
	}
	if(left){
		flip = 1;
		mVelX = -Mario_VEL; 
		animation = 2;
	}
	else if(right){
		flip = 0;
		mVelX = Mario_VEL;
		animation = 2;
	}
	else {
		mVelX = 0;
		animation = 1;
	}
	
	if(up){
		mVelY = -Mario_VEL; 
		animation = 3;
	}
	else if(down){
		mVelY = Mario_VEL;
		animation = 4;
	}
	else {
		mVelY = Mario_VEL;
	}
	
	if(mPosY <= maxJump){
		mVelY = Mario_VEL;
		up = 0;
		maxJump = 0;
	}
}

void Mario::move( SDL_Rect& wall )
{
    //Move the Mario left or right
    mPosX += mVelX;
    mCollider.x = mPosX;

    //If the Mario collided or went too far to the left or right
    if( ( mPosX < 0 ) || ( mPosX + Mario_WIDTH > SCREEN_WIDTH ) || checkCollision( mCollider, wall ) )
    {
        //Move back
        mPosX -= mVelX;
		  mCollider.x = mPosX;
    }

    //Move the Mario up or down
	 mPosY += mVelY;
	 mCollider.y = mPosY;

    //If the Mario collided or went too far up or down
    if( ( mPosY < 0 ) || ( mPosY + Mario_HEIGHT > SCREEN_HEIGHT ) || checkCollision( mCollider, wall ) )
    {
        //Move back
        mPosY -= mVelY;
		  mCollider.y = mPosY;
    }
}

void Mario::render()
{
    //Show the Mario
	gMarioTexture.render( mPosX, mPosY, NULL, NULL, NULL, flipType );
}

int Mario::getX()
{
	return mPosX;
}

int Mario::getY()
{
	return mPosY;
}

int Mario::getAnimation()
{
	return animation;
}

void Mario::setAnimation(int val)
{
	animation = val;
}

bool Mario::getFlip()
{
	return flip;
}

void Mario::setFlip(bool val)
{
	flip = val;
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Load press texture
	if( !gMarioTexture.loadFromFile( "../spritesheet.bmp" ) )
	{
		printf( "Failed to load Mario texture!\n" );
		success = false;
	}
	else
	{
		//Set sprite clips
		gSpriteClips[ 0 ].x = 13;
		gSpriteClips[ 0 ].y = 60;
		gSpriteClips[ 0 ].w = 17;
		gSpriteClips[ 0 ].h = 29;
		
		gSpriteClips[ 1 ].x = 30;
		gSpriteClips[ 1 ].y = 60;
		gSpriteClips[ 1 ].w = 17;
		gSpriteClips[ 1 ].h = 29;
		
		gSpriteClips[ 2 ].x = 47;
		gSpriteClips[ 2 ].y = 60;
		gSpriteClips[ 2 ].w = 17;
		gSpriteClips[ 2 ].h = 29;

		gSpriteClips[ 3 ].x = 30;
		gSpriteClips[ 3 ].y = 60;
		gSpriteClips[ 3 ].w = 17;
		gSpriteClips[ 3 ].h = 29;

		gSpriteClips[ 4 ].x = 155;
		gSpriteClips[ 4 ].y = 60;
		gSpriteClips[ 4 ].w = 17;
		gSpriteClips[ 4 ].h = 29;

		gSpriteClips[ 5 ].x = 215;
		gSpriteClips[ 5 ].y = 77;
		gSpriteClips[ 5 ].w = 15;
		gSpriteClips[ 5 ].h = 14;
	}
	
	return success;
}

void close()
{
	//Free loaded images
	gMarioTexture.free();

	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

bool checkCollision( SDL_Rect a, SDL_Rect b )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    //Calculate the sides of rect B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;

    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			//The Mario that will be moving around on the screen
			Mario Mario;

			//Current animation frame
			int frame = 0;

			//Set the wall
			SDL_Rect wall;
			wall.x = 50;
			wall.y = 175;
			wall.w = 425;
			wall.h = 20;
			
			SDL_RendererFlip flipType = SDL_FLIP_NONE;
			
			//While application is running
			while( !quit )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}

					//Handle input for the Mario
					Mario.handleEvent( e );
				}

				//Move the Mario and check collision
				Mario.move( wall );

				//Clear screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );

				if(Mario.getFlip())
					flipType = SDL_FLIP_HORIZONTAL;
				else
					flipType = SDL_FLIP_NONE;

				//Render current frame
				SDL_Rect* currentClip = &gSpriteClips[ 0 ];
				switch(Mario.getAnimation()){
					case 1:
						currentClip = &gSpriteClips[ 0 ];
						break;
					case 2:
						currentClip = &gSpriteClips[ frame / 4 ];
						break;
					case 3:
						currentClip = &gSpriteClips[ 4 ];
						break;
					case 4:
						currentClip = &gSpriteClips[ 5 ];
						break;
				}
				
				gMarioTexture.render( Mario.getX(), Mario.getY(), currentClip, NULL, NULL, flipType );

				//Render wall
				SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );		
				SDL_RenderDrawRect( gRenderer, &wall );
				
				//Render Mario
				//Mario.render();

				//Update screen
				SDL_RenderPresent( gRenderer );

				//Go to next frame
				++frame;

				//Cycle animation
				if( frame / 4 >= WALKING_ANIMATION_FRAMES )
				{
					frame = 0;
				}
			}
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}
